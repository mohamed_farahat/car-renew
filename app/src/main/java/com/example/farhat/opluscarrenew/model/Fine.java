package com.example.farhat.opluscarrenew.model;

public class Fine{

    String Title,date,numer,ammount;

    public Fine(String title, String date, String officer, String ammount) {
        Title = title;
        this.date = date;
        this.numer = officer;
        this.ammount = ammount;
    }

    public String getTitle() {
        return Title;
    }

    public String getDate() {
        return date;
    }

    public String getOfficer() {
        return numer;
    }

    public String getAmmount() {
        return ammount;
    }


    public void setTitle(String title) {
        Title = title;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setOfficer(String officer) {
        this.numer = officer;
    }

    public void setAmmount(String ammount) {
        this.ammount = ammount;
    }

}
