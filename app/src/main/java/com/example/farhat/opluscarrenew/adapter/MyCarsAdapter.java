package com.example.farhat.opluscarrenew.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.farhat.opluscarrenew.model.MyCars;
import com.example.farhat.opluscarrenew.R;

import java.util.ArrayList;

public class MyCarsAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<MyCars> cars;

    public MyCarsAdapter(Context context, ArrayList<MyCars> cars) {
        this.context = context;
        this.cars = cars;
    }

    @Override
    public int getCount() {
        return cars.size();
    }

    @Override
    public Object getItem(int position) {
        return cars.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(R.layout.my_vehicles_item, parent, false);

        TextView platenumbernumber = view.findViewById(R.id.platenumbernumber);
        TextView platenumberletters = view.findViewById(R.id.platenumberletters);

        TextView brand = view.findViewById(R.id.carbrand);
        TextView type = view.findViewById(R.id.cartype);
        TextView status = view.findViewById(R.id.licensestatus);

        platenumbernumber.setText(cars.get(position).getPlatenumbernumber());
        platenumberletters.setText(cars.get(position).getPlatenumberletter());

        brand.setText(cars.get(position).getBrand());
        type.setText(cars.get(position).getType());
        status.setText(cars.get(position).getStatus());

        switch (cars.get(position).getStatus()) {


            case "Valid":
                status.setTextColor(context.getResources().getColor(R.color.greenz));
                break;

            case "Warning":
                status.setTextColor(context.getResources().getColor(R.color.yellowz));
                break;

            case "Expired":
                status.setTextColor(context.getResources().getColor(R.color.redz));
                break;

        }

        return view;
    }
}
