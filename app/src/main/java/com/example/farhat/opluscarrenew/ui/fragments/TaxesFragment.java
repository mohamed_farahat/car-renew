package com.example.farhat.opluscarrenew.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.farhat.opluscarrenew.R;
import com.example.farhat.opluscarrenew.adapter.FinesAdapter;
import com.example.farhat.opluscarrenew.model.Fine;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class TaxesFragment extends Fragment {

    ListView taxeslist;

    public TaxesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_taxes, container, false);

        taxeslist = view.findViewById(R.id.taxes_list);

        ArrayList<Fine> fines = new <Fine>ArrayList();
        fines.add(new Fine("ضريبه 1", "11/12/2014", "ن د و  5 1 2", "33 L.E"));
        fines.add(new Fine("ضريبه 2", "11/12/2014", "ن د و  5 1 2", "23 L.E"));
        fines.add(new Fine("ضريبه 3", "11/12/2014", "ن د و  5 1 2", "213 L.E"));
        fines.add(new Fine("ضريبه 4", "11/12/2014", "ن د و  5 1 2", "55 L.E"));
        fines.add(new Fine("ضريبه 5", "11/12/2014", "ن د و  5 1 2", "34 L.E"));
        fines.add(new Fine("ضريبه 6", "11/12/2014", "ن د و  5 1 2", "66 L.E"));
        fines.add(new Fine("ضريبه 7", "11/12/2014", "ن د و  5 1 2", "45 L.E"));
        fines.add(new Fine("ضريبه 8", "11/12/2014", "ن د و  5 1 2", "34 L.E"));
        fines.add(new Fine("ضريبه 9", "11/12/2014", "ن د و  5 1 2", "32 L.E"));
        fines.add(new Fine("ضريبه 10", "11/12/2014", "ن د و  5 1 2", "545 L.E"));
        fines.add(new Fine("ضريبه 11", "11/12/2014", "ن د و  5 1 2", "44 L.E"));
        fines.add(new Fine("ضريبه 12", "11/12/2014", "ن د و  5 1 2", "234 L.E"));

        FinesAdapter myFinesAdapter = new FinesAdapter(getActivity(), fines);

        taxeslist.setAdapter(myFinesAdapter);

        return view;
    }
}
