package com.example.farhat.opluscarrenew.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.farhat.opluscarrenew.R;
import com.example.farhat.opluscarrenew.adapter.FinesAdapter;
import com.example.farhat.opluscarrenew.model.Fine;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class InsuranceFragment extends Fragment {

    ListView insurancelist;

    public InsuranceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_insurance, container, false);

        insurancelist = view.findViewById(R.id.insurance_list);

        ArrayList<Fine> fines = new <Fine>ArrayList();
        fines.add(new Fine("تامين 1", "11/12/2014", "ن د و  5 1 2", "23 L.E"));
        fines.add(new Fine("تامين 2", "11/12/2014", "ن د و  5 1 2", "12 L.E"));
        fines.add(new Fine("تامين 3", "11/12/2014", "ن د و  5 1 2", "421 L.E"));
        fines.add(new Fine("تامين 4", "11/12/2014", "ن د و  5 1 2", "321 L.E"));
        fines.add(new Fine("تامين 5", "11/12/2014", "ن د و  5 1 2", "23 L.E"));
        fines.add(new Fine("تامين 6", "11/12/2014", "ن د و  5 1 2", "312 L.E"));
        fines.add(new Fine("تامين 7", "11/12/2014", "ن د و  5 1 2", "553 L.E"));
        fines.add(new Fine("تامين 8", "11/12/2014", "ن د و  5 1 2", "32 L.E"));
        fines.add(new Fine("تامين 9", "11/12/2014", "ن د و  5 1 2", "32 L.E"));
        fines.add(new Fine("تامين 10", "11/12/2014", "ن د و  5 1 2", "23 L.E"));
        fines.add(new Fine("تامين 11", "11/12/2014", "ن د و  5 1 2", "31 L.E"));
        fines.add(new Fine("تامين 12", "11/12/2014", "ن د و  5 1 2", "312 L.E"));

        FinesAdapter myFinesAdapter = new FinesAdapter(getActivity(), fines);

        insurancelist.setAdapter(myFinesAdapter);

        return view;
    }

}
