package com.example.farhat.opluscarrenew.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.farhat.opluscarrenew.model.MyCars;
import com.example.farhat.opluscarrenew.adapter.MyCarsAdapter;
import com.example.farhat.opluscarrenew.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button addnew = findViewById(R.id.addnewvehicle);

        final ListView mycarslist = findViewById(R.id.myvehicles);

        ArrayList<MyCars> cars = new <MyCars>ArrayList();

        cars.add(new MyCars("123", "ا ب ج", "Type: Private", "Brand: KIA", "Valid"));
        cars.add(new MyCars("131", "ل و ن", "Type: Private", "Brand: BMW", "Warning"));
        cars.add(new MyCars("121", "و ن ل", "Type: Private", "Brand: Jeep", "Expired"));


        MyCarsAdapter myCarsAdapter = new MyCarsAdapter(MainActivity.this, cars);


        mycarslist.setAdapter(myCarsAdapter);

        mycarslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, CarsPaymentActivity.class);
                startActivity(intent);

            }
        });

        addnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  startActivity(new Intent(MainActivity.this, AddCarActivity.class));
            }
        });


    }

    @Override
    public void onBackPressed(){
        finish();
    }
}


