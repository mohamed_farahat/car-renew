package com.example.farhat.opluscarrenew.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.farhat.opluscarrenew.R;
import com.example.farhat.opluscarrenew.model.Fine;

import java.util.ArrayList;

public class FinesAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Fine> fines;

    public FinesAdapter(Context context, ArrayList<Fine> fines) {
        this.context = context;
        this.fines = fines;
    }

    @Override
    public int getCount() {
        return fines.size();
    }

    @Override
    public Object getItem(int position) {
        return fines.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(R.layout.fines_item, parent, false);

        TextView offencenumber = view.findViewById(R.id.finesplatenumebr);
        TextView plateNumber = view.findViewById(R.id.finesoffencenumber);
        TextView fineDate = view.findViewById(R.id.finesfinedate);
        TextView fineAmount = view.findViewById(R.id.finesfine_amount);

        offencenumber.setText(fines.get(position).getOfficer());
        plateNumber.setText(fines.get(position).getTitle());
        fineDate.setText(fines.get(position).getDate());
        fineAmount.setText(fines.get(position).getAmmount());

        return view;
    }
}
