package com.example.farhat.opluscarrenew.model;

public class MyCars {

    private String platenumbernumber,platenumberletter, type, brand, status;

    public MyCars(String platenumbernumber, String platenumberletter, String type, String brand, String status) {
        this.platenumbernumber = platenumbernumber;
        this.platenumberletter = platenumberletter;
        this.type = type;
        this.brand = brand;
        this.status = status;
    }

    public String getPlatenumbernumber() {
        return platenumbernumber;
    }

    public void setPlatenumbernumber(String platenumbernumber) {
        this.platenumbernumber = platenumbernumber;
    }

    public String getPlatenumberletter() {
        return platenumberletter;
    }

    public void setPlatenumberletter(String platenumberletter) {
        this.platenumberletter = platenumberletter;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
