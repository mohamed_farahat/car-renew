package com.example.farhat.opluscarrenew.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.example.farhat.opluscarrenew.R;
import com.example.farhat.opluscarrenew.ui.fragments.DeliveryFragment;
import com.example.farhat.opluscarrenew.ui.fragments.FinesFragment;
import com.example.farhat.opluscarrenew.ui.fragments.InsuranceFragment;
import com.example.farhat.opluscarrenew.ui.fragments.TaxesFragment;
import com.shuhart.stepview.StepView;

import java.util.ArrayList;

public class CarsPaymentActivity extends AppCompatActivity {
    StepView stepView;
    FragmentTransaction transaction;
    FinesFragment finesFragment;
    InsuranceFragment insuranceFragment;
    TaxesFragment taxesFragment;
    DeliveryFragment deliveryFragment;
    Fragment fragment;

    ProgressBar loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cars_payment);

        stepView = findViewById(R.id.step_view_fines);
        loadingBar = findViewById(R.id.loading_bar);
        handleStepView();

        finesFragment = new FinesFragment();
        insuranceFragment = new InsuranceFragment();
        taxesFragment = new TaxesFragment();
        deliveryFragment = new DeliveryFragment();

        handleFragmentTransaction(finesFragment);

    }

    public void payClicked(View view) {
        loadingBar.setVisibility(View.VISIBLE);
        fragment = this.getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof FinesFragment) {
            handleFragmentTransaction(insuranceFragment);
            stepView.go(1, true);
        }
        if (fragment instanceof InsuranceFragment) {
            handleFragmentTransaction(taxesFragment);
            stepView.go(2, true);
        }
        if (fragment instanceof TaxesFragment) {
            handleFragmentTransaction(deliveryFragment);
            stepView.go(3, true);
        }
        if (fragment instanceof DeliveryFragment) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    private void handleFragmentTransaction(Fragment fragment) {
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
    }

    private void handleStepView() {

        stepView.getState()
                .selectedTextColor(ContextCompat.getColor(this, R.color.colorAccent))
                .animationType(StepView.ANIMATION_LINE)
                .selectedCircleColor(ContextCompat.getColor(this, R.color.colorAccent))
                .selectedCircleRadius(getResources().getDimensionPixelSize(R.dimen.dp14))
                .selectedStepNumberColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .steps(new ArrayList<String>() {{
                    add("Fines");
                    add("Insurance");
                    add("taxes");
                    add("Delivery");
                }})
                .stepsNumber(4)
                .animationDuration(getResources().getInteger(android.R.integer.config_shortAnimTime))
                .stepLineWidth(getResources().getDimensionPixelSize(R.dimen.dp1))
                .textSize(getResources().getDimensionPixelSize(R.dimen.sp14))
                .stepNumberTextSize(getResources().getDimensionPixelSize(R.dimen.sp16))
                // other state methods are equal to the corresponding xml attributes
                .commit();
    }
}
