package com.example.farhat.opluscarrenew.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.farhat.opluscarrenew.R;
import com.example.farhat.opluscarrenew.adapter.FinesAdapter;
import com.example.farhat.opluscarrenew.model.Fine;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FinesFragment extends Fragment {

    ListView fineslist;

    public FinesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_fines, container, false);

        fineslist = view.findViewById(R.id.fines_list);

        ArrayList<Fine> fines = new <Fine>ArrayList();
        fines.add(new Fine("غرامه 1", "11/12/2014", "ن د و  5 1 2", "32 L.E"));
        fines.add(new Fine("غرامه 2", "11/12/2014", "ن د و  5 1 2", "55 L.E"));
        fines.add(new Fine("غرامه 3", "11/12/2014", "ن د و  5 1 2", "33 L.E"));
        fines.add(new Fine("غرامه 4", "11/12/2014", "ن د و  5 1 2", "23 L.E"));
        fines.add(new Fine("غرامه 5", "11/12/2014", "ن د و  5 1 2", "55 L.E"));
        fines.add(new Fine("غرامه 6", "11/12/2014", "ن د و  5 1 2", "321 L.E"));
        fines.add(new Fine("غرامه 7", "11/12/2014", "ن د و  5 1 2", "55 L.E"));
        fines.add(new Fine("غرامه 8", "11/12/2014", "ن د و  5 1 2", "32 L.E"));
        fines.add(new Fine("غرامه 9", "11/12/2014", "ن د و  5 1 2", "44 L.E"));
        fines.add(new Fine("غرامه 10", "11/12/2014", "ن د و  5 1 2", "33 L.E"));
        fines.add(new Fine("غرامه 11", "11/12/2014", "ن د و  5 1 2", "23 L.E"));

        FinesAdapter myFinesAdapter = new FinesAdapter(getActivity(), fines);

        fineslist.setAdapter(myFinesAdapter);

        return view;
    }

}
